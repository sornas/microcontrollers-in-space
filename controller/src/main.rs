#![no_std]
#![no_main]

use arduino_hal::delay_ms;
use arduino_hal::prelude::*;
use panic_halt as _;

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    let mut adc = arduino_hal::Adc::new(dp.ADC, Default::default());
    let a0 = pins.a0.into_analog_input(&mut adc);

    let btn1 = pins.d4.into_pull_up_input();
    let btn2 = pins.d3.into_pull_up_input();
    let btn3 = pins.d2.into_pull_up_input();

    let mut red = (
        pins.d5.into_output(),
        pins.d6.into_output(),
        pins.d7.into_output(),
        pins.d8.into_output(),
        pins.d9.into_output(),
    );
    let mut blue = (
        pins.d12.into_output(),
        pins.d11.into_output(),
        pins.d10.into_output(),
    );

    let mut serial = arduino_hal::default_serial!(dp, pins, 57600);

    loop {
        let val = a0.analog_read(&mut adc);
        serial.write_byte(1);
        serial.write_byte((val >> 2) as u8);
        if btn3.is_low() {
            serial.write_byte(2);
        }
        if btn2.is_low() {
            serial.write_byte(3);
        }
        if btn1.is_low() {
            serial.write_byte(4);
        }

        while let Ok(b) = serial.read() {
            if b == 0 {
                red.0.set_low();
                red.1.set_low();
                red.2.set_low();
                red.3.set_low();
                red.4.set_low();
            }
            if b == 10 {
                blue.0.set_low();
                blue.1.set_low();
                blue.2.set_low();
            }

            if b <= 5 {
                if b > 0 {
                    red.0.set_high();
                }
                if b > 1 {
                    red.1.set_high();
                }
                if b > 2 {
                    red.2.set_high();
                }
                if b > 3 {
                    red.3.set_high();
                }
                if b > 4 {
                    red.4.set_high();
                }
            } else {
                if b > 10 {
                    blue.0.set_high();
                }
                if b > 11 {
                    blue.1.set_high();
                }
                if b > 12 {
                    blue.2.set_high();
                }
            }
        }
        delay_ms(10);
    }
}
