use std::f32::consts::{PI, TAU};
use std::io::{Read, Write as _};

use macroquad::color::*;
use macroquad::math::Vec2;
use macroquad::shapes::{draw_circle, draw_rectangle_ex, DrawRectangleParams};
use macroquad::window::{
    clear_background, next_frame, screen_height, screen_width, set_fullscreen,
};
use macroquad_particles::{ColorCurve, Emitter, EmitterConfig};
use maths_rs::num::Base as _;
use maths_rs::vec::VecN;
use maths_rs::{dist, dot, normalize, rotate_2d, Vec2f};
use serialport::{SerialPort as _, TTYPort};
use sungod::Ra;

struct Bullet {
    pos: Vec2f,
    vel: Vec2f,
}

struct Player {
    port: Option<TTYPort>,
    pos: Vec2f,
    vel: Vec2f,
    rot: f32, // 0..=1
    bullets: Vec<Bullet>,
    dash_charge: f32,
    collision: bool,
    shoot: bool,
    amoo: f32,
    hp: f32,
    is_hit: bool,
    dead_timer: f32,
    engine_particles: Emitter,
    shoot_particles: Emitter,
}

struct State {
    p1: Player,
    p2: Player,
    last_frame: std::time::Instant,
}

fn engine_particles() -> Emitter {
    Emitter::new(EmitterConfig {
        local_coords: true,
        lifetime: 1.5,
        initial_velocity: 150.0,
        lifetime_randomness: 0.5,
        initial_direction: Vec2::new(1.0, 0.0),
        initial_direction_spread: 2.0 * std::f32::consts::PI,
        size: 3.0,
        size_randomness: 2.0,
        colors_curve: ColorCurve {
            start: GOLD,
            mid: RED,
            end: Color::new(0.0, 0.0, 0.0, 0.0),
        },
        emitting: false,
        ..Default::default()
    })
}

fn shoot_particles() -> Emitter {
    Emitter::new(EmitterConfig {
        local_coords: true,
        lifetime: 0.5,
        lifetime_randomness: 0.2,
        initial_direction: Vec2::new(1.0, 0.0),
        initial_direction_spread: 2.0 * std::f32::consts::PI,
        initial_velocity: 200.0,
        size: 4.0,
        size_randomness: 1.0,
        colors_curve: ColorCurve {
            start: Color::new(0.6, 0.85, 0.9, 1.0),
            mid: BLUE,
            end: Color::new(0.0, 0.0, 0.0, 0.0),
        },
        emitting: false,
        ..Default::default()
    })
}

#[macroquad::main("BasicShapes")]
async fn main() {
    let port1 = serialport::new("/dev/ttyACM0".to_string(), 57600)
        .timeout(std::time::Duration::from_secs(2))
        .open_native()
        .ok();
    let port2 = serialport::new("/dev/ttyACM1".to_string(), 57600)
        .timeout(std::time::Duration::from_secs(2))
        .open_native()
        .ok();

    set_fullscreen(true);

    let mut state = State {
        p1: Player {
            port: port1,
            pos: Vec2f::new(100.0, 100.0),
            vel: Vec2f::zero(),
            rot: 0.0,
            bullets: Vec::new(),
            dash_charge: 2.0,
            collision: false,
            shoot: false,
            amoo: 5.0,
            hp: 6.0,
            is_hit: false,
            dead_timer: 0.0,
            engine_particles: engine_particles(),
            shoot_particles: shoot_particles(),
        },
        p2: Player {
            port: port2,
            pos: Vec2f::new(400.0, 600.0),
            vel: Vec2f::zero(),
            rot: 0.0,
            bullets: Vec::new(),
            dash_charge: 2.0,
            collision: false,
            shoot: false,
            amoo: 5.0,
            hp: 6.0,
            is_hit: false,
            dead_timer: 0.0,
            engine_particles: engine_particles(),
            shoot_particles: shoot_particles(),
        },
        last_frame: std::time::Instant::now(),
    };

    loop {
        clear_background(BLACK);
        update(&mut state);

        draw(&mut state);
        next_frame().await
    }
}

fn draw_player(player: &mut Player, color: Color) {
    if player.dead_timer == 0.0 {
        draw_rectangle_ex(
            player.pos.x,
            player.pos.y,
            60.0,
            60.0,
            DrawRectangleParams {
                offset: Vec2 { x: 0.5, y: 0.5 },
                rotation: player.rot * TAU,
                color,
            },
        );
        let aim = player.pos + rotate_2d(Vec2f::unit_x() * 45.0, player.rot * TAU);
        draw_circle(aim.x, aim.y, 10.0, color);
    }
    for bullet in &player.bullets {
        draw_circle(bullet.pos.x, bullet.pos.y, 10.0, color);
    }
    player.engine_particles.draw(Vec2::ZERO);
    player.shoot_particles.draw(Vec2::ZERO);
}

fn draw(state: &mut State) {
    draw_player(&mut state.p1, BLUE);
    draw_player(&mut state.p2, RED);
}

fn update_player(player: &mut Player, target: Vec2f, delta: f32) {
    let mut byte = [0; 1];
    let Some(port) = &mut player.port else {
        return;
    };
    macro_rules! read_byte {
        () => {{
            port.read_exact(&mut byte).unwrap();
            byte[0]
        }};
    }

    player.shoot = false;

    while port.bytes_to_read().unwrap_or(0) > 0 {
        match read_byte!() {
            1 => {
                // new direction
                player.rot += ((read_byte!() as f32 / 256.0) - 0.5) / 50.0;
            }
            2 => {
                // acceleration
                let accel = rotate_2d(Vec2f::unit_x(), player.rot * TAU) * 3.0;
                player.vel += accel;
                player.engine_particles.emit(
                    Vec2::new(
                        player.pos.x - 40.0 * f32::sin(-player.rot * TAU + PI / 2.0)
                            + Ra::ggen::<f32>() * 10.0,
                        player.pos.y - 40.0 * f32::cos(-player.rot * TAU + PI / 2.0)
                            + Ra::ggen::<f32>() * 10.0,
                    ),
                    20,
                );
            }
            3 => {
                // shoot
                player.shoot = true;
            }
            4 => {
                // dash
                if player.dash_charge == 0.0 {
                    player.vel += rotate_2d(Vec2f::unit_x(), player.rot * TAU) * 160.0;
                    player.dash_charge = 5.0;
                }
            }
            _ => (),
        }
    }

    if player.shoot && player.dead_timer == 0.0 {
        if player.amoo > 0.0 && target != player.pos {
            let fv = rotate_2d(Vec2f::unit_x() * 40.0, player.rot * TAU);
            let begin_pos = player.pos + rotate_2d(Vec2f::unit_x() * 30.0, player.rot * TAU);
            let aim = normalize(target - begin_pos);
            if dot(fv, aim) > 0.0 {
                player.shoot_particles.emit(
                    Vec2::new(
                        player.pos.x
                            + 40.0 * f32::sin(-player.rot * TAU + PI / 2.0)
                            + Ra::ggen::<f32>() * 5.0,
                        player.pos.y
                            + 40.0 * f32::cos(-player.rot * TAU + PI / 2.0)
                            + Ra::ggen::<f32>() * 5.0,
                    ),
                    5,
                );
                player.amoo = (player.amoo - delta).max(0.0);
                player.bullets.push(Bullet {
                    pos: begin_pos,
                    vel: aim * (175.0 + dot(player.vel, aim)).max(100.0),
                });
            } else {
                player.shoot = false;
            }
        } else {
            player.shoot = false;
        }
    } else {
        player.amoo = (player.amoo + delta).min(5.0);
    }

    let amoo_led = player.amoo.div_euclid(1.0);
    port.write(&[0u8, amoo_led as u8]).unwrap();
    let hp_led = player.hp.div_euclid(2.0);
    port.write(&[10u8, 10 + hp_led as u8]).unwrap();

    player.dash_charge = 0.0f32.max(player.dash_charge - delta);
    player.vel = player.vel * 0.84f32.powf(delta);

    // update pos
    player.pos += player.vel * delta;
    // check pos inside walls
    let rot = (player.rot * TAU).rem_euclid(PI / 2.0);
    // ??????????
    let cornery = rotate_2d(Vec2f::new(30.0, 30.0), rot);
    let cornerx = rotate_2d(Vec2f::new(30.0, -30.0), rot);
    let maxx = player.pos.x + cornerx.x;
    let minx = player.pos.x - cornerx.x;
    let maxy = player.pos.y + cornery.y;
    let miny = player.pos.y - cornery.y;
    player.collision = minx < 0.0 || miny < 0.0 || maxx > screen_width() || maxy > screen_height();
    if player.collision {
        player.vel *= 0.5;
        // solve in a very dumb way, and mirror velocity
        if minx < 0.0 {
            player.pos += Vec2f::new(-minx, 0.0);
            player.vel.x *= -1.0;
        }
        if maxx > screen_width() {
            player.pos -= Vec2f::new(maxx - screen_width(), 0.0);
            player.vel.x *= -1.0;
        }
        if miny < 0.0 {
            player.pos += Vec2f::new(0.0, -miny);
            player.vel.y *= -1.0;
        }
        if maxy > screen_height() {
            player.pos -= Vec2f::new(0.0, maxy - screen_height());
            player.vel.y *= -1.0;
        }
    }

    for bullet in &mut player.bullets {
        bullet.pos += bullet.vel * delta;
    }
    player.bullets.retain(|b| {
        !(b.pos.x < -10.0
            || b.pos.y < -10.0
            || b.pos.x > screen_width() + 10.0
            || b.pos.y > screen_height() + 10.0)
    })
}

fn update(state: &mut State) {
    let this_frame = std::time::Instant::now();
    let delta = (this_frame - state.last_frame).as_secs_f32() * 5.0;
    state.last_frame = this_frame;
    update_player(&mut state.p1, state.p2.pos, delta);
    update_player(&mut state.p2, state.p1.pos, delta);

    state.p1.is_hit = false;
    state.p2.is_hit = false;

    // collision time
    for bullet in &state.p1.bullets {
        if dist(bullet.pos, state.p2.pos) < 10.0 + 30.0 {
            state.p2.is_hit = true;
            state.p2.hp -= delta * 0.4;
        }
    }

    for bullet in &state.p2.bullets {
        if dist(bullet.pos, state.p1.pos) < 10.0 + 30.0 {
            state.p1.is_hit = true;
            state.p1.hp -= delta * 0.4;
        }
    }

    if state.p1.is_hit && state.p1.hp < 0.0 {
        state.p1.dead_timer = 4.0;
    } else if !state.p1.is_hit && state.p1.dead_timer == 0.0 {
        state.p1.hp = (state.p1.hp + delta * 0.06).min(6.0);
    }

    if state.p2.is_hit && state.p2.hp < 0.0 {
        state.p2.dead_timer = 4.0;
    } else if !state.p2.is_hit && state.p2.dead_timer == 0.0 {
        state.p2.hp = (state.p2.hp + delta * 0.06).min(6.0);
    }

    if state.p1.dead_timer > 0.0 {
        state.p1.dead_timer -= delta;
        if state.p1.dead_timer <= 0.0 {
            state.p1.pos = Vec2f::new(200.0, screen_height() / 2.0);
            state.p1.dead_timer = 0.0;
            state.p1.hp = 6.0;
        }
    }

    if state.p2.dead_timer > 0.0 {
        state.p2.dead_timer -= delta;
        if state.p2.dead_timer <= 0.0 {
            state.p2.pos = Vec2f::new(screen_width() - 200.0, screen_height() / 2.0);
            state.p2.dead_timer = 0.0;
            state.p2.hp = 6.0;
        }
    }
}
